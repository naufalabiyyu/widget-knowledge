import 'package:flutter/material.dart';

class CustomFont extends StatelessWidget {
  const CustomFont({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Font'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Custom Font',
              style: TextStyle(fontFamily: 'Oswald', fontSize: 30),
            ),
            Text(
              'Oswald',
              style: TextStyle(fontFamily: 'Oswald', fontSize: 30),
            ),
          ],
        ),
      ),
    );
  }
}

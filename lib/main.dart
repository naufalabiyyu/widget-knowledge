import 'package:flutter/material.dart';
import 'package:widget_knowledge/custom_font/custom_font.dart';
import 'package:widget_knowledge/expanded_flexible/expanded_flexible.dart';
import 'package:widget_knowledge/expanded_flexible/expanded_widget.dart';
import 'package:widget_knowledge/input_widget/radio_widget.dart';
import 'package:widget_knowledge/input_widget/switch_widget.dart';
import 'package:widget_knowledge/input_widget/textbox_widget.dart';
import 'package:widget_knowledge/input_widget/textfield_widget.dart';
import 'package:widget_knowledge/listview_widget/listview_builder.dart';
import 'package:widget_knowledge/listview_widget/listview_dinamis.dart';
import 'package:widget_knowledge/listview_widget/listview_separated.dart';
import 'package:widget_knowledge/listview_widget/listview_widget.dart';
import 'package:widget_knowledge/navigation_widget/first_screen.dart';
import 'package:widget_knowledge/responsive_layout/layoutbuilder_widget.dart';
import 'package:widget_knowledge/responsive_layout/mediaquery_widget.dart';
import 'package:widget_knowledge/responsive_layout/responsive_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: Theme.of(context).copyWith(
        colorScheme: Theme.of(context).colorScheme.copyWith(
              primary: const Color(0xff00b4d8),
            ),
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppBar appBar() {
      return AppBar(
        title: Text('Widget Knowledge'),
      );
    }

    Widget content() {
      return ListView(
        scrollDirection: Axis.vertical,
        children: [
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  //* TextField Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TextFieldWidget(),
                          ),
                        );
                      },
                      child: const Text('TextField Widget'),
                    ),
                  ),
                  SizedBox(height: 8),
                  //* TextBox Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TextBoxWidget(),
                          ),
                        );
                      },
                      child: const Text('TextBox Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Switch Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SwitchWidget(),
                          ),
                        );
                      },
                      child: const Text('Switch Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Radio Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RadioWidget(),
                          ),
                        );
                      },
                      child: const Text('Radio Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Custom Font
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CustomFont(),
                          ),
                        );
                      },
                      child: const Text('Custom Font'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Lisview Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ListViewWidget(),
                          ),
                        );
                      },
                      child: const Text('ListView Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Lisview Dinamis
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ListViewDinamis(),
                          ),
                        );
                      },
                      child: const Text('ListView Dinamis'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Lisview Builder
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ListViewBuilder(),
                          ),
                        );
                      },
                      child: const Text('ListView Builder'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Lisview Separated
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ListViewSeparated(),
                          ),
                        );
                      },
                      child: const Text('ListView Separated'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Expanded Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ExpandedWidgetPage(),
                          ),
                        );
                      },
                      child: const Text('Expanded Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Expanded Flexible
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ExpandedFlexible(),
                          ),
                        );
                      },
                      child: const Text('Expanded Flexible'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Navigation Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => FirstScreen(),
                          ),
                        );
                      },
                      child: const Text('Navigation Widget'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* MediaQuery Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MediaQueryWidget(),
                          ),
                        );
                      },
                      child: const Text('MediaQuery'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* LayoutBuilder Widget
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LayoutBuilderWidget(),
                          ),
                        );
                      },
                      child: const Text('LayoutBuilder'),
                    ),
                  ),
                  const SizedBox(height: 8),
                  //* Responsive Page
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ResponsivePage(),
                          ),
                        );
                      },
                      child: const Text('Responsive Page'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: appBar(),
      body: content(),
    );
  }
}
